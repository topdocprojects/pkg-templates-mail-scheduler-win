Follow-up on getting patient reviews
<BODY style=font-size:12pt;font-family:Calibri>
<p>
    Dear *|DOCNAME|*,
</p>
<p>
    I hope you are well and safe.
</p>
<p>
I contacted you a couple of months ago about    <strong>patient reviews</strong>, and I just wanted to remind you about
    them again, as they are crucial to online positioning and the patient
    experience. If you would like to do either or both of the following please
    let me know:
</p>
<ul>
<li>
    Send me a list of patient emails and I will send them a review
    request email on your behalf.
</li>
<li>
    Re-send the patients already in your PatientConnect system a
    reminder to leave you a review on your behalf.
</li>
</ul>
<p>
    I am also on hand for any of the following:
</p>
<ul>
<li>
    Putting you in touch with our Content Team to collaborate on    <a href="https://www.topdoctors.co.uk/medical-articles">articles</a> and
    <a
        href="https://www.youtube.com/channel/UCQ7MbR2O0DyeT7uSSzJTu9w?view_as=subscriber"
    >
        videos
    </a>
</li>
    <li>
        Providing support for our
        <a href="https://www.youtube.com/watch?v=nTNFA9Yve48">
            e-Consultation
        </a>
        tool
    </li>
    <li>
        Setting up our
        <a href="https://www.youtube.com/watch?v=i_S2XiPABgQ">
            payment system
        </a>
        for pre-paid appointments
    </li>
<li>
    Making any changes or updates to your Top Doctors profile
</li>
</ul>
</BODY>